var express = require('express');
var app = express();

app.get('/', function(req, res){
    const excelToJson = require('convert-excel-to-json');
    const file1 = excelToJson({
        sourceFile: 'FeatureParameter1.xls'
    });

    const file2 = excelToJson({
        sourceFile: 'FeatureParameter2.xls'
    });
    
    //console.log(file1['Sheet1'].length);
    var acumText;

    for (var i=0; i<file1['Sheet1'].length; i++){
        var textoLimpio1 = JSON.stringify(file1['Sheet1'][i]);
        var textoLimpio2 = JSON.stringify(file2['Sheet1'][i]);
        if (textoLimpio1 != textoLimpio2){
            console.log ((i+1) + " " + textoLimpio1 + "\n" + (i+1) + " " + textoLimpio2 + "\n\n");
        }
        
    }
    
    res.status(200).send(acumText);

});

app.listen(3000);